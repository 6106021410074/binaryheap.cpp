template <class comparable>
void BinaryHeap<comparable>::insert(const comparable &x){
    if(isfull()){
        throw overflow();
    }
    int hole = ++currentsize;
    for(;hole>1 && x < array[hole/2]; hole = hole / 2){
        array[hole] = array[hole/2];
    }
    array[hole] = x;
}

template <class comparable>
void BinaryHeap<comparable>::deleteMin(comparable & minItem){
    if(isemtpy()){
        throw Underflow();
    }
    minItem = array[1];
    array[1] = array[currentSize--];
    percolateDown(1);
}

template <class comparable>
void BinaryHeap<comparable>::percolateDown(int hole){
    int child;
    comparable tmp = array[hole];
    for(;hole*2 <= currentsize ; hole = child){
        child = hole * 2;
        if(child != currentsize && array[child + 1] < array[child]){
            child ++ ;
        }
        if(array[child] < tmp){
            array[hole] = array[child];
        }
        else{
            break;
        }
    }
    array[hole] = tmp;
}